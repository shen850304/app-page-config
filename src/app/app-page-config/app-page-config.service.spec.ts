import { EmpItem } from './../viewmodel/emp';

import { TestBed } from '@angular/core/testing';
import {describe, expect, test} from '@jest/globals'
import { AppPageConfigService } from './app-page-config.service';
import { HttpClient , HttpEventType, HttpEvent} from '@angular/common/http';
import { of } from 'rxjs';
import { lastValueFrom } from 'rxjs';

describe('AppPageConfigService', () => {
  let service: AppPageConfigService;
  const http = new HttpClient({
    handle: ()=> of({} as HttpEvent<HttpEventType.Sent>)
  });

  beforeEach(() => {
    service = new AppPageConfigService(http);

  });

  //test getDevelopers()
  test('getDevelopers():should return employeeList', () => {

    //Arrage:期望達到的結果
    const expectList: EmpItem[]  = [
      {"empNo":3284,"empCode":"A3284 ","empName":"謝志宗"}];

    //Act:執行實際撰寫的方法
    //const actualList = await lastValueFrom(service.getDevelopers());
    let actualRes: EmpItem[] = [];
    service.getDevelopers().subscribe(x => actualRes = x );

    //Assert:斷言預期結果與實際值相同
    expect(actualRes).toEqual(expectList);

  });
});

