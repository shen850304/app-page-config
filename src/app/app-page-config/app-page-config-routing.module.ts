import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AppPageConfigComponent } from './app-page-config.component';

const routes: Routes = [
  {
    path: '',
    component: AppPageConfigComponent
  }

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PageRoutingModule { }
