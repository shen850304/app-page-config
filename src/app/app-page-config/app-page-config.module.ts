import { MessageModule } from 'primeng/message';
import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AppPageConfigComponent } from './app-page-config.component';
import { PageRoutingModule } from './app-page-config-routing.module';
import { AppPageConfigService } from './app-page-config.service';
import { AppPageConfigToolbarComponent } from '../app-page-config-toolbar/app-page-config-toolbar.component';
import {ButtonModule} from 'primeng/button';
import {TableModule} from 'primeng/table';
import { AppPageConfigTableComponent } from '../app-page-config-table/app-page-config-table.component';
import {TabViewModule} from 'primeng/tabview';
import { FormsModule } from '@angular/forms';
import { InputTextModule } from 'primeng/inputtext';
import {CheckboxModule} from 'primeng/checkbox';
import {MultiSelectModule} from 'primeng/multiselect';
import { OverlayModule } from '@angular/cdk/overlay';
import {DialogModule} from 'primeng/dialog';
import { DropdownModule } from 'primeng/dropdown';
import {ToastModule} from 'primeng/toast';
import { OverlayPanelModule } from 'primeng/overlaypanel';

@NgModule({
  declarations: [
    AppPageConfigComponent,
    AppPageConfigToolbarComponent,
    AppPageConfigTableComponent
  ],
  imports: [
    CommonModule,
    PageRoutingModule,
    ButtonModule,
    TableModule,
    TabViewModule,
    FormsModule,
    InputTextModule,
    CheckboxModule,
    MultiSelectModule,
    OverlayModule,
    HttpClientModule,
    DialogModule,
    DropdownModule,
    ToastModule,
    OverlayPanelModule
  ],
  exports:[
    AppPageConfigComponent
  ],
  providers:[
    AppPageConfigService
  ]

})
export class AppPageConfigModule { }
