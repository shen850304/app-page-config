import { PageItem } from './../viewmodel/page';
import { Component, OnInit } from '@angular/core';
import { AppPageConfigService } from './app-page-config.service';

@Component({
  selector: 'app-page-config',
  templateUrl: './app-page-config.component.html',
  styleUrls: ['./app-page-config.component.scss']
})

export class AppPageConfigComponent{
  public pageData!:PageItem;
  public modifyInfos:PageItem[]=[];
  public appPages:PageItem[]=[];
  public isSave:boolean =false;

/**
 *將傳入的資訊更新至pageData的值
 * @param $event 型態為pageItem
 *
 */
  doupdate($event:PageItem){
    this.pageData = $event;
    }

/**
 *將傳入的資訊更新至modifyInfos的值
 * @param $event 型態為pageItem[]
 */
  doCheckSave($event:PageItem[]){
    this.modifyInfos = $event;
  }

  /**
   * 取得appPage的資料
   * @param $event
   */
  getAppPageInfos($event:PageItem[]){
    this.appPages = $event;
  }

  getCheckSave($event:boolean){
    this.isSave = $event;
    console.log("config isSave"+this.isSave);
  }
}
