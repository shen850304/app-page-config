import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AppPageConfigComponent } from './app-page-config.component';

describe('AppPageConfigComponent', () => {
  let component: AppPageConfigComponent;
  let fixture: ComponentFixture<AppPageConfigComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AppPageConfigComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AppPageConfigComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
