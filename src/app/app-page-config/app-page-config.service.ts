import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { lastValueFrom, Observable } from 'rxjs';
import { EmpItem } from '../viewmodel/emp';
import { PageItem } from '../viewmodel/page';

@Injectable({
  providedIn: 'root'
})
export class AppPageConfigService {

  public appPageInfos: PageItem[] =[]
  public empInfos: EmpItem[] =[]
  constructor(private http: HttpClient) { }

  /**
   * 取得應用頁面資訊(api版)
   * @returns appPageInfos : 回傳應用頁面資訊
   */
  public getAppPage(){
    const apiUrl = 'https://his-alpha.cmuh.org.tw/webapi/appStoreManager/appMenu/getAppPageInfos';//api url
    const apiContent = lastValueFrom(this.http.get<PageItem[]>(apiUrl));//api content
    // this.appPageInfos = apiContent.map(value => ({
    //     "pageId": value.pageId,
    //     "pageTitle": value.pageTitle,
    //     "templateUrl": value.templateUrl?value.templateUrl:'null',
    //     "versionId": value.versionId,
    //     "fontSize": value.fontSize,
    //     "remoteName": value.remoteName? value.remoteName: 'null',
    //     "moduleName": value.moduleName? value.moduleName: 'null',
    //     "maintainerNo": value.maintainerNo,
    //     "maintainerCode":value.maintainerCode,
    //     "maintainerName":value.maintainerName,
    //     "systemUser":value.systemUser,
    //     "isUse":value.isUse,
    //     "entityState":value.entityState
    // }));
    return apiContent;
  }

    /**
   *
   *取得員工資訊(api版)
   * @return empInfos：取得api的資訊後，並map至empItem的欄位，回傳map過後的資訊
   * @memberof AppPageConfigService
   */
  public  getDevelopers(): Observable<EmpItem[]>{
    const apiUrl = 'https://his-alpha.cmuh.org.tw/webapi/appStoreManager/appMenu/getDevelopers';//api url
    const apiContent = this.http.get<EmpItem[]>(apiUrl);//api content
    return apiContent;
  }

  /**
   * 將新增或修改的頁面資訊傳至api：setAppPageInfos，未有此項目則新增，已有此項目則更新，並回傳結果
   * @return apiRes：回傳要新增或修改的資訊
   * @memberof AppPageConfigService
   */
  public  setModifyInfos(values:PageItem[]){
    const apiUrl = 'https://his-alpha.cmuh.org.tw/webapi/appStoreManager/appMenu/setAppPageInfos';
    const apiRes = this.http.put(apiUrl, values);
    console.log("要修改的資料:"+values);
    return apiRes;
  }

  /**
   *
   * @param values for json版的
   * @returns
   */
  // public  setModifyInfos(values:PageItem[]){
  //   const apiRes = values;
  //   return apiRes;
  // }

  /**
   *取得該頁面的api，查詢是否有被使用
   * @param  inputId：number： 傳入值為查詢頁面的pageId
   * @return apiContent： 回傳字串，未被使用則回傳空字串，有被使用則回傳該頁面是被哪個單位使用
   * @memberof AppPageConfigService
   */
  public getIsUsed(inputId:number){
    const api = "https://his-alpha.cmuh.org.tw/webapi/appStoreManager/appMenu/checkAppPageUsed/"+inputId; //api資料
    // const api = "../../assets/checkAppUsed/"+inputId+".txt";//測試資料用
    const apiContent = lastValueFrom(this.http.get<string>(api));
    return apiContent;
  }

  /**
   *使用page.json的資料，用於未連內網時的測試版
   *取得應用頁面資訊(json版)
   * @return appPageInfos：回傳json中的資料
   * @memberof AppPageConfigService
   */
  // public getAppPage(){
  //   const jsonUrl = '../../assets/page.json';
  //   const apiContent = lastValueFrom(this.http.get<PageItem[]>(jsonUrl));
  //   return apiContent;
  // }

  /**
   *使用emp.json的資料，用於未連內網時的測試版
   *取得員工資訊(json版)
   * @return empInfos：回傳json內的資料
   * @memberof AppPageConfigService
   */
  // public getDevelopers(){
  //   const jsonUrl = '../../assets/emp.json';
  //   const apiContent = lastValueFrom(this.http.get<EmpItem[]>(jsonUrl));
  //   return apiContent;
  // }




}
