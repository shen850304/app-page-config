export interface EmpItem {
  "empNo": number,
  "empCode": string,
  "empName": string
}
