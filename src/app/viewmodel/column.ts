export interface ColumnItem {
  "title": string,
  "id": string,
  "width": number,
  "isChoose":boolean,
  "isDefault":boolean
}
