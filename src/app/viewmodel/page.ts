export interface PageItem {
  pageId: number,
  pageTitle: string,
  templateUrl: string,
  versionId: string,
  fontSize: number,
  remoteName: string,
  moduleName: string,
  maintainerNo: number | null,
  maintainerCode:string,
  maintainerName:string,
  systemUser:string,
  isUse:boolean,
  entityState:string

}
