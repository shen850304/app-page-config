import { PageItem } from './../viewmodel/page';
import { Component, Input, OnInit, EventEmitter, Output } from '@angular/core';
import { AppPageConfigService } from '../app-page-config/app-page-config.service';
import { LazyLoadEvent, PrimeNGConfig } from 'primeng/api';
import  columns  from '../../assets/columns.json';
import { ColumnItem } from '../viewmodel/column';
import { HttpClient } from '@angular/common/http';
import {MessageService} from 'primeng/api';

@Component({
  selector: 'app-page-config-table',
  templateUrl: './app-page-config-table.component.html',
  styleUrls: ['./app-page-config-table.component.scss'],
  providers :[MessageService]
})

export class AppPageConfigTableComponent implements OnInit {
  public pages:PageItem[]=[];
  public updateItem:PageItem[]=[];
  public lastPage:PageItem[]=[];
  public originPage:PageItem[]=[];
  public titles:ColumnItem[]=[];
  public selectItems:ColumnItem[]=[];
  public clonedPage: { [s: string]: PageItem; } = {};
  public inputId:number = 0;
  public search:string='';
  public apiContent:string ='';
  public msg:string='';
  public checkItem:number=0;
  public num :number[]=[];
  public modifyInfos:PageItem[]=[];
  public checkUpdate :boolean=false;
  public loading: boolean =false;

  constructor(public appPageConfigSvc:AppPageConfigService,private primengConfig: PrimeNGConfig,private http: HttpClient,private messageService: MessageService) {
  }

  /**
   *input : 從子元件toolbar->父元件config->再傳入此元件，得出新增後的全部資訊
   *
   * @type {PageItem[]}
   * @memberof AppPageConfigTableComponent
   */
  @Input()
  pageData !:PageItem;

  @Input()
  isSave !:boolean;

  @Output()
  appPage = new EventEmitter<PageItem[]>();

  @Output()
  update = new EventEmitter<PageItem[]>();

  /**
   *當checkUpdate=true代表參數page包含在變數modifyInfos內，將其emit至輸出值update中
   * @param {PageItem} page
   * @return checkUpdate：帶入值page已是否存在在變數modifyInfos中，回傳的結果
   * checkUpdate的結果為true，則將modifyInfos emit至輸出值update中，並回傳結果
   * @memberof AppPageConfigTableComponent
   */
  public changeColor(page:PageItem){
    this.checkUpdate = this.modifyInfos.includes(page);
    if(this.checkUpdate==true){
      this.update.emit(this.modifyInfos);
    }
    return this.checkUpdate;
  }

  /**
   * 取得初始狀態：ngOnInit生命週期，發生在初始化ngOnChange後
   * 1. 變數title等於columns的資訊
   * 2. 篩選出title中的欄位isChoose為true的項目並等於變數selectItems
   * 3. 取得頁面資訊api的資訊，變數名為originPage
   * 4. 讓變數lastPage的值等於originPage
   */
  public ngOnInit(){
    this.primengConfig.ripple = true;
    this.titles=columns;
    this.selectItems=this.titles.filter(x => x.isChoose==true);
    this.getAppPage();
    console.log("table 初始化");
  }

  /**
   * 紀錄table有輸入值新增資料，一發生改變，將輸入值unshift至page資料內
   * ngOnChanges生命週期，發生在初始化建構子後 & 輸出入變動時
   * 如果傳入值 pageData的值不為null，則
   *   1. 將傳入值加入到lastPage的最前面
   *   2. 將傳入值加入到modifyInfos的最後面
   *   3. 將originPage的值更新成lastPage
   * 如果傳入值 isSave為true，則
   *   1.
   */
  public  ngOnChanges() {
    if(this.pageData!=null) {
      this.isSave==false;
      console.log("table.isSave"+this.isSave);
      this.lastPage.unshift(this.pageData);
      this.modifyInfos.push(this.pageData);
      this.update.emit(this.modifyInfos);
      this.originPage = this.lastPage;
     }

     if(this.isSave==true){
       this.getAppPage();
       this.lastPage = this.originPage;
       console.log("儲存後更新表格成功");
       this.modifyInfos.length=0;
       console.log("table modifyInfos="+this.modifyInfos);
       this.pageData == null;

     }
     else{
       console.log("table isSave=false");
     }
  }

/**
 * 取得appPage的資訊，並將appPage emit至@Output:appPage
 */
  public async getAppPage(){
    this.originPage = await this.appPageConfigSvc.getAppPage();
    this.appPage.emit(this.originPage);
    console.log("重新取得page資料");
    this.orderPage();
   }

   private  orderPage() {
    this.originPage.sort(function (a, b) {
      if (a > b) return 1;
      if (a < b) return -1;
      return 0;
    });
    console.log("排序資料");
  }
  /**
   *
   *等待載入時，表格會顯示正在載入的狀態
   * @param {LazyLoadEvent} event
   * @memberof AppPageConfigTableComponent
   */
  public loadPage(event: LazyLoadEvent){
    this.loading = true;
    setTimeout(() => {
      if (this.originPage) {
          this.lastPage = this.originPage.slice(event.first);
          this.loading = false;
      }
  }, 1000);

  }

  /**
   *查詢特定的頁面資訊，目前輸入文字需全部符合頁面的名稱
   *如果查詢值不為''，lastPage值為originPage的值且會篩選出lastPage中pageTitle包含查詢值的項目
   *查詢值為''，lastPage值為originPage的值
   * @memberof AppPageConfigTableComponent
   */
  searchTitle(){
    if(this.search!='') {
      this.lastPage = this.originPage;
      this.lastPage = this.lastPage.filter((item)=>{return item.pageTitle === this.search});
    }
    else this.lastPage = this.originPage;
  }

  /**
   *轉換成該列編輯模式
   * @param page 型態PageItem
   * clonedPage值為參數page的頁面資訊
   */
  onRowEditInit(page:PageItem){
    this.clonedPage[page.pageId] = {...page};

   }

   /**
    *儲存該列編輯狀態
    * @param page 型態PageItem
    * @param index 型態為number
    * 1. 刪除clonedPage的資訊
    * 2. 變數lastPage中的第 [index]個的entityState='Modified'
    * 3. 將變數lastPage中的第 [index]個 的資訊加入至modifyInfos
    * 4. 並將原本的clonedPage的pageId更新至變數num內
    */
   onRowEditSave(page:PageItem, index: number){
     delete this.clonedPage[page.pageId];
     this.lastPage[index].entityState='Modified';
     this.modifyInfos.push(this.lastPage[index]);
     this.update.emit(this.modifyInfos);
     this.num.push(page.pageId);
   }

   /**
    *取消編輯狀態並回復原狀態
    * @param page 型態為PageItem
    * @param index 型態為number
    * 1. 變數lastPage的第[index]個值變更為clonedPage的pageId
    * 2. 刪除掉變數lastPage 欄位為pageId
    * 3. console.log
    */
   onRowEditCancel(page:PageItem, index: number){
     this.lastPage[index] = this.clonedPage[page.pageId];
   }

   /**
    *刪除應用頁面資訊檔
    *1. 先抓取api資料，確認是否有應用程式正在使用該資訊檔，使用service中的方法：getIsUsed()取得該資訊
    *2. 如果有資料，則會跳出訊息，顯示目前有哪些應用程式正在使用該資訊檔，並不刪除
    *3. 若沒有資料，則刪除該資訊檔
    * @param page
    */
   async deletePage(page:PageItem){
    this.inputId = page.pageId;
    this.apiContent = await this.appPageConfigSvc.getIsUsed(this.inputId);
    if(this.apiContent != ''){
      this.msg='對不起，應用頁面資訊檔'+this.apiContent;
      this.messageService.add({key: 'br',closable:false, severity:'warn', detail: this.msg});
    }
    else{
      this.lastPage = this.lastPage.filter((item)=>{ return item !== page});
      this.num.push(this.inputId);
      page.entityState='Deleted';
      this.modifyInfos.push(page);
      this.update.emit(this.modifyInfos);
    }
  }

}
