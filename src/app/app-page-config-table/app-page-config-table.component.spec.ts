import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AppPageConfigTableComponent } from './app-page-config-table.component';

describe('AppPageConfigTableComponent', () => {
  let component: AppPageConfigTableComponent;
  let fixture: ComponentFixture<AppPageConfigTableComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AppPageConfigTableComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AppPageConfigTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
