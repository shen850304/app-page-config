import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    redirectTo:'/page',
    pathMatch:'full'
  },
  {
      path: 'page',
      loadChildren: () => import('./app-page-config/app-page-config.module')
          .then(m => m.AppPageConfigModule)
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
