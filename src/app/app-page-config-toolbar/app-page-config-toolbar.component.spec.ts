import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AppPageConfigToolbarComponent } from './app-page-config-toolbar.component';

describe('AppPageConfigToolbarComponent', () => {
  let component: AppPageConfigToolbarComponent;
  let fixture: ComponentFixture<AppPageConfigToolbarComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AppPageConfigToolbarComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AppPageConfigToolbarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
