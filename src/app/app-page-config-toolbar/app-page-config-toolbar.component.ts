import { PageItem } from './../viewmodel/page';
import { EmpItem } from './../viewmodel/emp';
import { Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import { AppPageConfigService } from '../app-page-config/app-page-config.service';
import { MessageService } from 'primeng/api';
import { last, lastValueFrom } from 'rxjs';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-page-config-toolbar',
  templateUrl: './app-page-config-toolbar.component.html',
  styleUrls: ['./app-page-config-toolbar.component.scss'],
  providers: [MessageService],
})
export class AppPageConfigToolbarComponent {
  ShowErrorMsg: any;

  constructor(
    public appPageConfigSvc: AppPageConfigService,
    private messageService: MessageService,
    private http: HttpClient
  ) {
    this.getDeveloper();
  }

  @Output()
  update = new EventEmitter<PageItem>();

  @Output()
  isSave = new EventEmitter<boolean>();

  @Input()
  appPages !: PageItem[];

  @Input()
  lastPage!: PageItem;

  @Input()
  modifyInfos!: PageItem[];

  public position: string = '';
  public employees: EmpItem[] = [];
  public selectemp!: EmpItem;
  public appPage: PageItem[] = [];
  public lastData: PageItem[] = [];
  public addData: PageItem[] = [];
  public ans: boolean = false;
  public count: number = 0;
  public alert: boolean = false;
  public alerturl: boolean = false;
  public displayPosition: boolean = false;
  public notComplete: boolean = true;
  public notUpdate: boolean = true;
  public newPage: PageItem = {
    pageId: 0,
    pageTitle: '',
    templateUrl: '',
    versionId: '1.0.0',
    fontSize: 12,
    remoteName: '',
    moduleName: '',
    maintainerNo: null,
    maintainerCode: '',
    maintainerName: '',
    systemUser: '',
    isUse: false,
    entityState: '',
  };

  /**
   * ngOnchanges生命週期，發生在初始化建構子之後&輸出入值有變動時
   * 如果input：modifyInfos有內容，變數notUpdate值為false，反之變數notUpdate值為true
   * 如果input：appPages有內容，讓appPage=其值，並排序取得最後一個值
   */
  public ngOnChanges() {
    if (this.modifyInfos.length != 0) this.notUpdate = false;
    else this.notUpdate = true;
    this.appPage = this.appPages;

  }


    /**
   * 1. 從API中取得所有員工名單的資訊，變數名：employees；從API中取得頁面資訊，變數名：appPage
   * 2. 如果傳入值：lastPage有內容，則將傳入值加回原本陣列，並依小到大排序
   * 3. 取得變數appPage的最後一筆資訊，變數名：lastData
   * 4. 更新變數newPage.pageId的值，為lastData[0].pageId+1
   */
  private  orderPage() {
   if (this.lastPage != null) this.appPage.push(this.lastPage);
   this.appPage.sort(function (a, b) {
     if (a > b) return 1;
     if (a < b) return -1;
     return 0;
   });
   this.lastData = this.appPage.slice(-1);
   this.newPage.pageId = this.lastData[0].pageId + 1;
 }

 /**
  * 取得員工名單
  */
  public async getDeveloper() {
    this.employees = await lastValueFrom(this.appPageConfigSvc.getDevelopers());
    // this.appPageConfigSvc.getDevelopers().subscribe(x => this.employees = x);//了解rxjs，上面的另一種非同步寫法
  }



  /**
   * 刷新表單頁面
   */
  private flashPage(){
    this.newPage = {
      pageId: this.lastData[0].pageId + 1,
      pageTitle: '',
      templateUrl: '',
      versionId: '1.0.0',
      fontSize: 12,
      remoteName: '',
      moduleName: '',
      maintainerNo: null,
      maintainerCode: '',
      maintainerName: '',
      systemUser: '',
      isUse: false,
      entityState: '',
    }
  }


  /**
   *
   *測試表單是否填寫完整，完整則將變數notComplete設為false，對應到則是確認按鈕可以按
   */
  public onCheckCompleteBlur() {
    if (
      this.newPage.pageId != null &&
      this.newPage.remoteName != '' &&
      this.newPage.moduleName != '' &&
      this.newPage.pageTitle != '' &&
      this.newPage.templateUrl != '' &&
      this.selectemp.empCode != ''
    )
      this.notComplete = false;
    else this.notComplete = true;
  }

  /**
   * 如果變數notComplete值為false，則
   *   1. 改變displayPosition、newPage的值
   *   2. 將更新的newPage emit至輸出值 update
   *   3. 執行newPage()，以更新newPage的值
   */
  public async onSubmitClick() {
    if (this.notComplete == false) {
      this.displayPosition = false;
      this.newPage.maintainerCode = this.selectemp.empCode;
      this.newPage.maintainerName = this.selectemp.empName;
      this.newPage.maintainerNo = this.selectemp.empNo;
      this.newPage.entityState = 'Added';
      this.update.emit(this.newPage);
      this.flashPage();
    }
  }

  /**
   * 顯示新增應用頁面對話框的出現位置
   * @param position，型態為string用來指定顯示位置
   */
  public onInsertClick(position: string) {
    this.position = position;
    this.displayPosition = true;
    this.orderPage();

  }

  /**
   * @param value 傳入值為pageId
   * 1. 執行onCheckCompleteBlur，測試表單是否填寫完整
   * 2. 傳入值是否有在appPage的pageId中出現，結果true，變數alert=true，則會出現提醒 重複
   */
  public onTestpageBlur(value: any) {
    this.onCheckCompleteBlur();
    this.ans = this.appPage.some((x) => x.pageId == value);
    if (this.ans != true) this.alert = false;
    else this.alert = true;
  }

  /**
   *
   * @param value 傳入值為templateUrl
   * 1. 執行onCheckCompleteBlur，測試表單是否填寫完整
   * 2. 傳入值是否有存在在appPage的templateUrl中，結果true，變數alerturl=true，則會出現提醒 重複
   */
  public onTestUrlBlur(value: any) {
    this.onCheckCompleteBlur();
    this.ans = this.appPage.some((x) => x.templateUrl == value);
    if (this.ans != true) this.alerturl = false;
    else this.alerturl = true;
  }

  /**
   * 1. 執行flashPage()
   * 2. displayPosition=false
   */
  public onCancelClick() {
    this.flashPage();
    this.displayPosition = false;
  }

  /**
   *
   * @param values 型態為PageItem[]
   * try
   *   1. 將values帶入setModifyInfos的方法並回傳結果，變數apiRes
   *   2. 如果apiRes有值，顯示存檔成功的訊息，反之則出現存檔失敗的訊息
   * catch
   *   帶入setModifyInfos的結果出錯，顯示錯誤訊息
   */
  public async onSaveClick(values: PageItem[]) {
    try {
      const apiRes = await lastValueFrom(this.appPageConfigSvc.setModifyInfos(values));
      if (apiRes) {
        this.isSave.emit(true);
        this.modifyInfos.length =0;
        this.messageService.add({
          severity: 'success',
          summary: '存檔成功',
          detail: `恭喜您存檔成功`,
          life: 3000,
        });
      } else {
        this.messageService.add({
          severity: 'warn',
          summary: '存檔失敗',
          detail: `對不起，存檔作業忙碌中，請稍後存檔，謝謝。`,
          life: 3000,
        });
      }
    } catch (err) {
      this.ShowErrorMsg('對不起!「應用資訊頁面」發生錯誤，請聯繫維護人員。');
    }
  }
}

